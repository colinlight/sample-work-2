//
//  DialViewController.m
//  speedshift-ui-prototype
//
//  Created by Colin Light on 15/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import "DialViewController.h"
#import "DiscView.h"

@interface DialViewController ()

@end

@implementation DialViewController

@synthesize discView;

@synthesize backView;

@synthesize bpmLabel;


//static int DISC_SIZE = 600;

static int DISC_SIZE = 400;


#pragma mark - life cycle
-(id)init
{
    self = [super init];
    if ( self )
    {
        
        DiscView *disc = [[DiscView alloc]initWithFrame:CGRectMake(0, 0, DISC_SIZE, DISC_SIZE)];
        disc.delegate = self;
        self.discView = disc;
        [disc release];
        [self.view addSubview:self.discView];
        
        
        UIImageView *bg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"disc-bg"]];
        CGRect bgFrame = bg.frame;
        bgFrame.origin.y = self.view.frame.size.height - bgFrame.size.height;
        bg.frame = bgFrame;
        [self.view addSubview:bg];
        
        [bg setUserInteractionEnabled:YES];
        [bg setMultipleTouchEnabled:YES];
        
        self.backView = bg;
       
        [self.view sendSubviewToBack:self.backView];
        [bg release];
        
        
        
        
        UILabel *readoutLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 - 100/2,
                                                                         50, 100, 100)];
        self.bpmLabel = readoutLabel;
        self.bpmLabel.textAlignment = NSTextAlignmentCenter;
        [readoutLabel release];
        self.bpmLabel.text = @"TEST";
        [self.view addSubview:self.bpmLabel];

        

        
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self
                                                                                    action:@selector(dialTouched:)];
        [self.backView addGestureRecognizer:panGesture];
        [panGesture release];
        
        
    
        
        
        mRotation = 0;
        

    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
	self.view.backgroundColor = [UIColor greenColor];
    
    CGRect discFrame = self.discView.frame;

    discFrame.origin.x = self.view.frame.size.width/2;
    //discFrame.origin.y = self.view.frame.size.height/2;
    discFrame.origin.y = self.view.frame.size.height + 350;
    self.discView.frame = discFrame;
    
    
    NSLog(@"test h w %f %f ", self.view.frame.size.width, discFrame.size.width);
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidUnload
{
    [super viewDidUnload];
    self.discView = nil;
    self.backView = nil;
    self.bpmLabel = nil;
}

-(void)dealloc
{
    [discView release];
    [backView release];
    [bpmLabel release];
    [super dealloc];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
   [discView stopRotation];
}

#pragma mark - public methods
-(IBAction)dialTouched:(UIPanGestureRecognizer*)sender
{
    
    CGPoint panPoint = [sender locationInView:self.discView ];
   
    if ( [sender state] == UIGestureRecognizerStateBegan )
        [discView startUpdatingDialForLocX:panPoint.x forLocY:panPoint.y];
    
    [discView updateDialForLocX:panPoint.x forLocY:panPoint.y ];
    
    
    if ( [sender state] == UIGestureRecognizerStateEnded )
    {
        CGPoint velPoint = [sender velocityInView:self.discView];
        [discView dialUpdateCompleteWithVelocity:velPoint];
    }
}

#pragma mark - DiscViewDelegate methods
-(void)selectedIndexChanged:(int)index
{
    NSString *bpm = [NSString stringWithFormat:@"%i BMP", index];
    self.bpmLabel.text = bpm;
}



@end
