//
//  DiscView.m
//  speedshift-ui-prototype
//
//  Created by Colin Light on 15/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "DiscView.h"

@interface DiscView ()
-(void)initLayout;
-(void)setFirstPointStartAngle;
-(void)positionDotsForAngle:(float)angle;
-(void)positionAViewForAngle:(float)angle;
-(float)easeOutQuadWithTime:(float)t withStartValue:(float)b withChange:(float)c withDuration:(float)d;
-(float)linearTweenWithTime:(float)t withStartValue:(float)b withChange:(float)c withDuration:(float)d;
@end

@implementation DiscView

@synthesize testDot;

@synthesize dotDict;

@synthesize rotationState;

@synthesize startDate;

@synthesize delegate;

@synthesize selectedIndex;

static int RADIUS = 500;
static int SEGEMENTS = 38;

//static int RADIUS = 150;
//static int SEGEMENTS = 10;

static inline UIImageView * getDotImage( NSString* dotName){
       return [[[UIImageView alloc] initWithImage:[UIImage imageNamed:dotName]] autorelease];
}

static inline UILabel * getValueLabel(NSString* value){
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = [UIColor clearColor];
    label.text = value;
    return [label autorelease];
}

static inline float degreesToRadians(degrees){
    return (M_PI * degrees)/180;
}

static inline float radiansToDegrees(float radians){
    return (radians*180)/M_PI;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initLayout];
        
        [self stopRotation];
        
        [NSTimer scheduledTimerWithTimeInterval:0.01
                                         target:self
                                       selector:@selector(rotateDial)
                                       userInfo:nil
                                        repeats:YES];
    }
    return self;
}


-(void)dealloc
{
    [testDot release];
    [super dealloc];
}


#pragma mark - public methods
-(void)stopRotation
{
    self.rotationState = RotationStateOff;
    
    //if selected position is not at -90 then we must rotate the dial unitl it in place
    UIView *selectedView = [self.dotDict objectAtIndex:selectedIndex];
    float angle = atan2(selectedView.center.y, selectedView.center.x);
    if ( angle != -90 )
    {
        
    }
}


-(void)startUpdatingDialForLocX:(float)xPos forLocY:(float)yPos
{
    self.rotationState = RotationStateUser;
    mStartTheta = atan2(yPos, xPos);
    [self setFirstPointStartAngle];
}

-(void)updateDialForLocX:(float)xPos forLocY:(float)yPos
{
    CGFloat theta = atan2(yPos, xPos);
    float difference =  mStartTheta - theta;
    
    float angle = mFirstPointStartAngle - difference;
    [self positionDotsForAngle:angle];
    
    
    //TESTING
//    UIView *test = [self.dotDict objectAtIndex:1];
//    float testAngle = atan2(test.center.y, test.center.x);
//    NSLog(@"Fisrt Angle: %f", radiansToDegrees(testAngle));
}


-(void)dialUpdateCompleteWithVelocity:(CGPoint)velocity
{
    
    if (( velocity.x < 30 ) && ( velocity.x > -30 ))
    {
        [self stopRotation];
        return;
    }
    
    NSDate *date = [[NSDate alloc]init];
    self.startDate = date;
    [date release];
    
   

    //NSLog(@"VEL X: %f", velocity.x);

    mTotalDist = degreesToRadians(360);
    
    //velocity = change / duration
    //change = endPostion - startPosition
    //THEREFORE
    //change = velocity * duration
    
    
    static int MAX_DURATION = 2;
    
    mDirection = ( velocity.x < 0 )? 1 : -1;
    mDuration = 1;  // == d NOTE: this will change depending on how large the swip velocity is
    mBeginningPosition = degreesToRadians(2); //beginning postion == b
    float endPostion = 0;
    mChange = endPostion - mBeginningPosition;
    
    
    //NSLog(@"velocity.x %f", velocity.x);
    
    mDuration = ( mChange * velocity.x ) /50;
    
    if ( mDuration < 0 )
        mDuration = mDuration * -1;
    
    mDuration = ( mDuration > MAX_DURATION )? MAX_DURATION : mDuration;
    
    //NSLog(@"mDuration %f", mDuration);

    self.rotationState = RotationStateAnimation;
    
}



-(void)rotateDial
{
    if ( self.rotationState != RotationStateAnimation )
        return;
    
    
    float currentTime = [[NSDate date]timeIntervalSinceDate:self.startDate];
    
     //float theta = [self linearTweenWithTime:currentTime withStartValue:mBeginningPosition withChange:mChange withDuration:mDuration];
    float theta = [self easeOutQuadWithTime:currentTime withStartValue:mBeginningPosition withChange:mChange withDuration:mDuration];
    //NSLog(@"theta %f", theta);
    
    if ( theta <= 0.0001 )
         [self stopRotation];
    
    
    [self setFirstPointStartAngle];
    
    CGFloat finishAngle = mFirstPointStartAngle - ( theta * mDirection );
    
    [self positionDotsForAngle:finishAngle];    
}



#pragma mark - private methods
-(void)initLayout
{
    float diameter = RADIUS * 2;
    UIView *guideX = [[UIView alloc]initWithFrame:CGRectMake(0, -RADIUS, 2, diameter)];
    guideX.backgroundColor = [UIColor redColor];
    [self addSubview:guideX];
    [guideX release];
    
    UIView *guideY = [[UIView alloc]initWithFrame:CGRectMake(-RADIUS, 0, diameter, 2)];
    guideY.backgroundColor = [UIColor redColor];
    [self addSubview:guideY];
    [guideY release];
    
    //-------------------------------
    mStartOffSet = degreesToRadians(-90);
    mSegmentSeperation = degreesToRadians(360) / SEGEMENTS;
    mLowerSelectedAngle = mStartOffSet + mSegmentSeperation/2;
    mUpperSelectedAngle = mStartOffSet - mSegmentSeperation/2;
    
    NSLog(@"Selected bound Angle: %f %f %f", radiansToDegrees(mStartOffSet),
          radiansToDegrees(mLowerSelectedAngle), radiansToDegrees(mUpperSelectedAngle));


    NSMutableArray *list = [[NSMutableArray alloc]initWithCapacity:SEGEMENTS];
    self.dotDict = list;
    [list release];
    

    float angle = mStartOffSet;
    for ( int i=0; i < SEGEMENTS; i++) {
        
        NSString *name = ( i == 0 )? @"startdot" : @"dot";
        UIImageView *dot = getDotImage( name );
        
        NSString *key = [NSString stringWithFormat:@"%d", i];
        
        [self.dotDict addObject:dot];
        
        float dx = cosf( angle ) * RADIUS;
        float dy = sinf( angle ) * RADIUS;
        
        dot.center = CGPointMake(dx, dy);
        
        //--------------
    
        float dx2 = cosf( angle ) * ( RADIUS - 20 );
        float dy2 = sinf( angle ) * ( RADIUS - 20 );
        
        UILabel *valueLabel = getValueLabel( key );
        valueLabel.frame = CGRectMake(-10, 10, 20, 20);
        valueLabel.frame = CGRectOffset(valueLabel.frame, dx2, dy2);
        
        if ( i == 0 )
        {
            dot.alpha = 0.3;
            self.testDot = dot;
        }
        
         //--------------
        
        [self addSubview:dot];
        [self addSubview:valueLabel];
        
        angle +=mSegmentSeperation;
    }
    
    [self bringSubviewToFront:self.testDot];

    mAngle = 0;
}

-(void)setFirstPointStartAngle
{
    UIView *firstDot = [self.dotDict objectAtIndex:0];
    mFirstPointStartAngle = atan2(firstDot.center.y, firstDot.center.x);
}


-(void)positionDotsForAngle:(float)angle
{
    int i = 0;
    
    for (UIView *dot in self.dotDict) {
        
        float dx = cosf( angle ) * RADIUS;
        float dy = sinf( angle ) * RADIUS;
        dot.center = CGPointMake(dx, dy);
        angle+= mSegmentSeperation;

        //need to use atan to get the test angle as the rotation
        //angle will provide incorrect values
        float testAngle = atan2(dot.center.y, dot.center.x);
        
        dot.alpha = 1;
        
        if (( testAngle > mUpperSelectedAngle ) && ( testAngle < mLowerSelectedAngle ) )
        {
            selectedIndex = i;
            dot.alpha = 0.5;
        }
        i++;
    }
    
    [self.delegate selectedIndexChanged:selectedIndex];
}


-(void)positionAViewForAngle:(float)angle
{
    NSLog(@"Angle: %f", radiansToDegrees(angle));
    
    float dx = cosf( angle ) * RADIUS;
    float dy = sinf( angle ) * RADIUS;
    self.testDot.center = CGPointMake(dx, dy);
}


-(float)easeOutQuadWithTime:(float)t withStartValue:(float)b withChange:(float)c withDuration:(float)d
{
    t /= d;
    return -c * t*(t-2) + b;
}

-(float)linearTweenWithTime:(float)t withStartValue:(float)b withChange:(float)c withDuration:(float)d
{
    return c*t/d + b;
}






@end
