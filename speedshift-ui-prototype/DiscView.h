//
//  DiscView.h
//  speedshift-ui-prototype
//
//  Created by Colin Light on 15/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DiscViewDelegate <NSObject>

-(void)selectedIndexChanged:(int)index;

@end

typedef NS_ENUM(NSInteger, RotationState){
    RotationStateOff,
    RotationStateUser,
    RotationStateAnimation
};

@interface DiscView : UIView
{
    float mAngle;
    UIView *testDot;
    NSMutableArray *dotDict;
    CGFloat mStartTheta;
    CGFloat mFirstPointStartAngle;
    RotationState rotationState;
    NSDate *startDate;
    
    float mStartOffSet;
    float mSegmentSeperation;
    float mLowerSelectedAngle;
    float mUpperSelectedAngle;
    
    int mDirection;
    float mDuration;
    float mSpeed;
    float mTotalDist;
    float mBeginningPosition;
    float mChange;
    
    int selectedIndex;
}

@property ( nonatomic, retain ) UIView *testDot;

@property ( nonatomic, retain ) NSMutableArray *dotDict;

@property ( nonatomic, assign ) RotationState rotationState;

@property ( nonatomic, retain ) NSDate *startDate;

@property ( nonatomic, assign ) id<DiscViewDelegate> delegate;

@property ( nonatomic, assign ) int selectedIndex;

-(void)stopRotation;

-(void)startUpdatingDialForLocX:(float)xPos forLocY:(float)yPos;

-(void)updateDialForLocX:(float)xPos forLocY:(float)yPos;

-(void)dialUpdateCompleteWithVelocity:(CGPoint)velocity;

-(void)rotateDial;

@end
