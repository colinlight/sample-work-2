//
//  DialViewController.h
//  speedshift-ui-prototype
//
//  Created by Colin Light on 15/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiscView.h"


@interface DialViewController : UIViewController<DiscViewDelegate>
{
    DiscView *discView;
    CGPoint mStartPosition;
    int mRotation;
    UIImageView *backView;
    UILabel *bpmLabel;
}

@property ( nonatomic, retain ) DiscView *discView;

@property ( nonatomic, retain ) UIImageView *backView;

@property ( nonatomic, retain ) UILabel *bpmLabel;

-(IBAction)dialTouched:(UIPanGestureRecognizer*)sender;

@end
